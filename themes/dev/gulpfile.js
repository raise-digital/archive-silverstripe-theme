
var config = {};

config.name = '{{ PROJECT }}';
config.port = 9000;
config.proxy = '{{ project.fdp.local }}';

config.path = require('path');
config.fs = require('fs');

config.dir = process.env.PWD;

config.project = config.path.basename(config.path.resolve(config.dir, '../../'));
config.theme = config.path.basename(config.dir).replace('dev', config.name);

config.src = config.path.resolve(config.dir, 'src');
config.dest = config.path.resolve(config.dir, '..', config.theme);

config.assets = [
  // {
  //   src: `${config.src}/fonts/**`,
  //   dest: `${config.dest}/fonts`,
  // }
];
config.sassIncludePaths = [];
config.jsEntries = {
  Page: ['./page.js']
};

const themeGulp = require('@fdp/silverstripe-theme-gulp');

themeGulp(config);
