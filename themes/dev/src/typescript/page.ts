import * as $ from 'jquery';

import { Page } from './pages/page';

$(document).ready(function() {
    (new Page()).onLoad();
});
